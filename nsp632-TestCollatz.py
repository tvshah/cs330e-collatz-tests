#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------
from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve
# -----------
# TestCollatz
# -----------

class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "143 1022\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 143)
        self.assertEqual(j, 1022)

    def test_read_2(self):
        s = "9832 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 9832)
        self.assertEqual(j, 10)

    def test_read_3(self):
        s = "34726 894278\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 34726)
        self.assertEqual(j, 894278)

    def test_read_4(self):
        s = "8372 332244\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 8372)
        self.assertEqual(j, 332244)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        #Lower end of range
        v = collatz_eval(1, 1201)
        self.assertEqual(v, 182)

    def test_eval_2(self):
        #Mid range
        v = collatz_eval(628478, 234892)
        self.assertEqual(v, 509)

    def test_eval_3(self):
        #Coerner case: Upper end of range
        v = collatz_eval(999999, 789879)
        self.assertEqual(v, 525)

    def test_eval_4(self):
        #Mid range
        v = collatz_eval(424424, 528479)
        self.assertEqual(v, 470)

    def test_eval_5(self):
        #i and j same values
        v = collatz_eval(12312, 12312)
        self.assertEqual(v, 38)

    def test_eval_6(self):
        # i > j
        v = collatz_eval(53727, 2781)
        self.assertEqual(v, 340)

    def test_eval_7(self):
        # i > j
        v = collatz_eval(37492, 23429)
        self.assertEqual(v, 324)

    def test_eval_8(self):
        #Large range between i and j
        v = collatz_eval(538, 123990)
        self.assertEqual(v, 354)




    # -----
    # print
    # -----
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 53727, 2781, 340)
        self.assertEqual(w.getvalue(), "53727 2781 340\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 589, 2273, 183)
        self.assertEqual(w.getvalue(), "589 2273 183\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 93187, 83718, 333)
        self.assertEqual(w.getvalue(), "93187 83718 333\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 128, 37, 119)
        self.assertEqual(w.getvalue(), "128 37 119\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("12312 12312\n34232 234\n673188 571888\n128 37\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "12312 12312 38\n34232 234 308\n673188 571888 509\n128 37 119\n")

    def test_solve_2(self):
        r = StringIO("93187 83718\n589 2273\n53727 2781\n1132 231231\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "93187 83718 333\n589 2273 183\n53727 2781 340\n1132 231231 443\n")

    def test_solve_3(self):
        r = StringIO("21313 23231\n638828 533828\n842431 324229\n133435 224882\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "21313 23231 269\n638828 533828 509\n842431 324229 525\n133435 224882 386\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
